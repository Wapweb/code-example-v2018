<?php

namespace BeeJee\Framework\View;

/**
 * Class DefaultView
 * @package BeeJee\Framework\View
 */
class DefaultView implements ViewInterface
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string|null
     */
    private $layout;

    /**
     * @var string|null
     */
    private $content;

    /**
     * DefaultView constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param string $name
     * @param array $params
     * @return string
     * @throws \Throwable
     */
    public function render(string $name, array $params = []): string
    {
        $level = ob_get_level();
        $templateFileName = $this->getTemplateFileName($name);

        $content = $this->renderTemplate($templateFileName, $level, $params);

        if (!$this->layout) {
            return $content;
        }

        $this->content = $content;
        $layout = $this->layout;
        $this->layout = null;

        return $this->render($layout, $params);
    }

    /**
     * @param string $name
     * @param array $params
     * @return string
     */
    public function partial(string $name, array $params = []): string
    {
        $level = ob_get_level();
        $templateFileName = $this->getTemplateFileName($name);

        return $this->renderTemplate($templateFileName, $level, $params);
    }

    /**
     * @param string $name
     */
    public function setLayout(string $name): void
    {
        $this->layout = $name;
    }

    /**
     * @return null|string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $string
     * @return string
     */
    public function encode($string): string
    {
        return htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE);
    }


    /**
     * @param string $name
     * @return string
     */
    private function getTemplateFileName(string $name): string
    {
        return $this->path.'/'.$name.'.phtml';
    }

    /**
     * @param string $fileName
     * @param int $level
     * @param array $params
     * @return string
     * @throws \Throwable
     */
    private function renderTemplate(string $fileName, int $level, array $params = []): string
    {
        try {
            ob_start();
            extract($params, EXTR_OVERWRITE);
            require $fileName;
            $content = ob_get_clean();
        } catch (\Throwable|\Exception $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }
            throw $e;
        }

        return $content;
    }
}
