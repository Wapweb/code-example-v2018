<?php

namespace BeeJee\Framework\View;

/**
 * Interface ViewInterface
 * @package BeeJee\Framework\View
 */
interface ViewInterface
{
    /**
     * @param string $name
     * @param array $params
     * @return string
     */
    public function render(string $name, array $params = []): string;
}
