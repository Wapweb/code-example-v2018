<?php

namespace BeeJee\Framework\Validator;

interface ValidatorInterface
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value): bool;
}
