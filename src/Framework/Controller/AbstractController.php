<?php

namespace BeeJee\Framework\Controller;

use BeeJee\Framework\View\ViewInterface;

/**
 * Class AbstractController
 * @package BeeJee\Framework\Controller
 */
class AbstractController
{
    /**
     * @var ViewInterface
     */
    protected $view;

    /**
     * AbstractController constructor.
     * @param ViewInterface $view
     */
    public function __construct(ViewInterface $view)
    {
        $this->view = $view;
    }

    /**
     * @param int $currentPage
     * @param int $totalItems
     * @param int $perPage
     * @return bool
     */
    protected function pageIsValid($currentPage, $totalItems, $perPage)
    {
        $maxPages = ceil($totalItems / $perPage);
        if (!$maxPages) {
            $maxPages = 1;
        }
        if ($currentPage < 1 || $currentPage > $maxPages) {
            return false;
        }

        return true;
    }
}
