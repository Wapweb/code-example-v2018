<?php

namespace BeeJee\Framework;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class Application
 * @package BeeJee\Framework
 */
class Application
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var RequestHandlerInterface
     */
    private $requestHandler;

    /**
     * Application constructor.
     * @param ContainerInterface $container
     * @param RequestHandlerInterface $requestHandler
     */
    public function __construct(ContainerInterface $container, RequestHandlerInterface $requestHandler)
    {
        $this->container = $container;
        $this->requestHandler = $requestHandler;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->requestHandler->handle($request);

        return $response;
    }
}
