<?php

namespace BeeJee\Framework\Form;

use BeeJee\Framework\Validator\ValidatorInterface;

interface FormInterface extends ValidatorInterface
{
    /**
     * @return mixed
     */
    public function getValues();

    public function getErrors(): array;
}
