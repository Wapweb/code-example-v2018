<?php

namespace BeeJee\Framework\Db\Sql;

trait PdoCrudTrait
{
    /**
     * @param \Pdo $pdo
     * @param string $tableName
     * @param array $data
     * @return string
     */
    protected function insertData(\Pdo $pdo, string $tableName, array $data)
    {
        $query = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $fields = array_keys($data);

        $columnsPart = implode(',', $fields);
        $valuesPlaceholders = array_map(function ($field) {
            return sprintf(':%s', $field);
        }, $fields);
        $valuesPlaceholders = implode(',', $valuesPlaceholders);

        $preparedQuery = sprintf($query, $tableName, $columnsPart, $valuesPlaceholders);

        $statement = $pdo->prepare($preparedQuery);
        foreach ($data as $field => $value) {
            $statement->bindParam(sprintf(':%s', $field), $value);
        }

        $statement->execute();

        return $pdo->lastInsertId();
    }

    /**
     * @param \Pdo $pdo
     * @param string $tableName
     * @param array $data
     * @param array|null $where
     * @return bool
     */
    protected function updateData(\Pdo $pdo, string $tableName, array $data, array $where = null): bool
    {
        $query = 'UPDATE `%s` SET %s';
        $updateParts = [];
        $values = [];
        foreach ($data as $field => $value) {
            $values[] = $value;
            $updateParts[] = sprintf('`%s` = ?', $field);
        }
        $preparedQuery = sprintf($query, $tableName, implode(', ', $updateParts));

        if (empty($where)) {
            $statement = $pdo->prepare($preparedQuery);

            return $statement->execute($values);
        }

        $values = array_merge($values, array_values($where));
        $query = '%s WHERE %s';
        $wherePart = $this->buildWherePart($where);
        $preparedQuery = sprintf($query, $preparedQuery, $wherePart);
        $statement = $pdo->prepare($preparedQuery);

        return $statement->execute($values);
    }

    /**
     * @param array $where
     * @return string
     */
    private function buildWherePart(array $where): string
    {
        $whereParts = [];
        foreach ($where as $field => $value) {
            $whereParts[] = sprintf('`%s` = ?', $field);
        }

        return implode('AND ', $whereParts);
    }

    /**
     * @param \Pdo $pdo
     * @param string $tableName
     * @param array|null $where
     * @return bool
     */
    protected function deleteData(\Pdo $pdo, string $tableName, array $where = null): bool
    {
        $query = 'DELETE FROM `%s`';
        $preparedQuery = sprintf($query, $tableName);
        if (empty($where)) {
            return $pdo->prepare($preparedQuery)->execute();
        }
        $values = array_values($where);
        $query = '%s WHERE %s';
        $wherePart = $this->buildWherePart($where);
        $preparedQuery = sprintf($query, $preparedQuery, $wherePart);
        $statement = $pdo->prepare($preparedQuery);

        return $statement->execute($values);
    }

}
