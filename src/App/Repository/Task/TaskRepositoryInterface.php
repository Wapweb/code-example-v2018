<?php

namespace BeeJee\App\Repository\Task;

use BeeJee\App\Model\Task\Task;

interface TaskRepositoryInterface
{
    public function save(Task $task);

    public function delete(Task $task);

    public function getAllByCriteria(GetAllCriteria $getAllCriteria);

    public function getAllCountByCriteria(GetAllCriteria $getAllCriteria);

    public function getOneById($id) : Task;
}
