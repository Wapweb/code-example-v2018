<?php

namespace BeeJee\App\Repository\Task;

use BeeJee\App\Model\Task\Task;
use BeeJee\Framework\Db\Sql\PdoCrudTrait;

/**
 * Class JsonFileTaskRepository
 * @package BeeJee\App\Repository\Task
 */
class JsonFileTaskRepository implements TaskRepositoryInterface
{
    /**
     * @var string
     */
    private $taskDbFileName;

    /**
     * JsonFileTaskRepository constructor.
     * @param string $taskDbFileName
     */
    public function __construct(string $taskDbFileName)
    {
        $this->taskDbFileName = $taskDbFileName;
    }

    /**
     * @param Task $task
     * @return bool|int
     */
    public function save(Task $task)
    {
        if ($task->getId()) {
            return $this->update($task);
        }

        return $this->add($task);
    }

    /**
     * @param Task $task
     * @return bool|int
     * @throws NotFoundException
     */
    private function update(Task $task)
    {
        $items = $this->getDbItems();

        $foundItemIndex = $this->getItemIndexByTaskId($items, $task);

        $taskData = $task->toArray();
        $items[$foundItemIndex] = $taskData;

        return file_put_contents($this->taskDbFileName, json_encode($items));
    }

    /**
     * @return array|mixed
     */
    private function getDbItems()
    {
        if (!is_file($this->taskDbFileName)) {
            return [];
        }

        $tasksData = file_get_contents($this->taskDbFileName);
        if (empty($tasksData)) {
            return [];
        }

        return json_decode($tasksData, true);
    }

    /**
     * @param array $items
     * @param Task $task
     * @return int|string
     * @throws NotFoundException
     */
    private function getItemIndexByTaskId(array $items, Task $task)
    {
        if (empty($items)) {
            throw new NotFoundException();
        }

        foreach ($items as $i => $item) {
            if ($item['id'] === $task->getId()) {
                return $i;
            }
        }

        throw new NotFoundException();
    }

    /**
     * @param Task $task
     * @return int|bool
     */
    public function add(Task $task)
    {
        $newId = uniqid();
        $task->setId($newId);
        $data = $task->toArray();
        $items = $this->getDbItems();
        $items[] = $data;

        return file_put_contents($this->taskDbFileName, json_encode($items));
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function delete(Task $task)
    {
        $items = $this->getDbItems();

        $foundItemIndex = $this->getItemIndexByTaskId($items, $task);
        unset($items[$foundItemIndex]);

        return file_put_contents($this->taskDbFileName, json_encode($items));
    }

    /**
     * @param GetAllCriteria $getAllCriteria
     * @return Task[]
     */
    public function getAllByCriteria(GetAllCriteria $getAllCriteria)
    {
        $items = $this->getDbItems();
        $items = $this->filterItemsBCriteria($items, $getAllCriteria);

        return $this->createModels($items);
    }

    /**
     * @param array $items
     * @param GetAllCriteria $getAllCriteria
     * @param bool $forCount
     * @return Task[]
     */
    private function filterItemsBCriteria(array $items, GetAllCriteria $getAllCriteria, $forCount = false)
    {
        if (empty($items)) {
            return $items;
        }
        $sort = $getAllCriteria->getSort();

        if ($sort && !$forCount) {
            usort($items, function (array $a, array $b) use ($sort) {
                switch ($sort) {
                    case GetAllCriteria::SORT_USERNAME_DESC:
                        return strcmp($a['username'], $b['username']);
                    case GetAllCriteria::SORT_STATUS_DESC:
                        return $a['status'] < $b['status'];
                    case GetAllCriteria::SORT_EMAIL_DESC:
                        return strcmp($a['email'], $b['email']);
                    default:
                        return $a['id'] < $b['id'];
                }
            });
        }

        if (!$sort) {
            $items = array_reverse($items);
        }

        $page = $getAllCriteria->getPage();
        $perPage = $getAllCriteria->getPerPage();

        if (!$page || $forCount) {
            return $items;
        }

        return array_slice($items, ($page - 1) * $perPage, $perPage);
    }

    /**
     * @param array $items
     * @return Task[]
     */
    private function createModels(array $items)
    {
        if (empty($items)) {
            return [];
        }

        return array_map(function (array $item) {
            return $this->createModel($item);
        }, $items);
    }

    /**
     * @param array $item
     * @return Task
     */
    private function createModel(array $item)
    {
        return Task::fromParams($item);
    }

    /**
     * @param GetAllCriteria $getAllCriteria
     * @return int
     */
    public function getAllCountByCriteria(GetAllCriteria $getAllCriteria)
    {
        $items = $this->getDbItems();
        $items = $this->filterItemsBCriteria($items, $getAllCriteria, true);

        return count($items);
    }

    /**
     * @param string $id
     * @return Task
     * @throws NotFoundException
     */
    public function getOneById($id): Task
    {
        $items = $this->getDbItems();
        foreach ($items as $item) {
            if ($item['id'] == $id) {
                return $this->createModel($item);
            }
        }

        throw new NotFoundException();
    }
}
