<?php

namespace BeeJee\App\Repository\Task;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Exception\InvalidArgumentException;

/**
 * Class GetAllCriteria
 * @package BeeJee\App\Repository\Task
 */
class GetAllCriteria
{
    public const
        SORT_USERNAME_DESC = 'username',
        SORT_EMAIL_DESC = 'email',
        SORT_STATUS_DESC = 'status';

    /**
     * @var int|null
     */
    private $page;

    /**
     * @var string|null
     */
    private $sort;

    /**
     * @var int
     */
    private $perPage;

    /**
     * GetAllCriteria constructor.
     * @param string|null $sort
     * @param int|null $page
     * @param int $perPage
     */
    public function __construct(string $sort = null, int $page = null, int $perPage = 3)
    {
        $this->sort = $this->validateSort($sort);
        $this->page = $this->validatePage($page);
        $this->perPage = $this->validatePerPage($perPage);
    }

    /**
     * @param string|null $sort
     * @return string
     */
    private function validateSort(string $sort = null)
    {
        if ($sort !== null
            && !in_array($sort, [self::SORT_EMAIL_DESC, self::SORT_STATUS_DESC, self::SORT_USERNAME_DESC])) {
            throw new InvalidArgumentException();
        }

        return $sort;
    }

    /**
     * @param int|null $page
     * @return int
     */
    private function validatePage(int $page = null)
    {
        if ($page !== null && $page < 1) {
            throw new InvalidArgumentException();
        }

        return $page;
    }

    /**
     * @param int $perPage
     * @return int
     */
    private function validatePerPage(int $perPage)
    {
        if ($perPage < 1 || $perPage > 1000) {
            throw new InvalidArgumentException();
        }

        return $perPage;
    }

    /**
     * @param ServerRequestInterface $request
     * @return GetAllCriteria
     */
    public static function fromRequest(ServerRequestInterface $request)
    {
        $queryParams = $request->getQueryParams();

        $page = $queryParams['page'] ?? 1;
        $sort = $queryParams['sort'] ?? null;

        return new self($sort, $page);
    }

    /**
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return null|string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }
}
