<?php

namespace BeeJee\App\Service\Task;

use BeeJee\App\Model\Task\AddTaskRequest;
use BeeJee\App\Model\Task\Task;
use BeeJee\App\Repository\Task\TaskRepositoryInterface;
use BeeJee\App\Service\ImageFileManager;
use BeeJee\App\Service\ImageFileManagerInterface;
use BeeJee\Framework\Form\FormInterface;
use Zend\Diactoros\UploadedFile;

/**
 * Class CreateTaskService
 * @package BeeJee\App\Task\Service
 */
class AddTaskService
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var FormInterface
     */
    private $addTaskValidator;

    /**
     * @var ImageFileManagerInterface
     */
    private $imageFileManager;

    private $imageWidth = 320;

    private $imageHeight = 240;

    /**
     * CreateTaskService constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param FormInterface $addTaskValidator
     * @param ImageFileManagerInterface $imageFileManager
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository,
        FormInterface $addTaskValidator,
        ImageFileManagerInterface $imageFileManager
    ) {
        $this->taskRepository = $taskRepository;
        $this->addTaskValidator = $addTaskValidator;
        $this->imageFileManager = $imageFileManager;
    }

    /**
     * @param AddTaskRequest $taskRequest
     * @return Task
     * @throws AddTaskValidationException
     */
    public function execute(AddTaskRequest $taskRequest)
    {
        $taskRequestValid = $this->addTaskValidator->isValid($taskRequest->toArray());
        if (!$taskRequestValid) {
            throw new AddTaskValidationException();
        }

        $imageName = $this->saveImage($taskRequest);

        $formValues = $this->addTaskValidator->getValues();
        $formValues['image'] = $imageName;
        $formValues['status'] = Task::STATUS_INIT;

        $task = Task::fromParams($formValues);
        $this->taskRepository->save($task);

        return $task;
    }

    /**
     * @param AddTaskRequest $taskRequest
     * @return string
     */
    private function saveImage(AddTaskRequest $taskRequest)
    {
        $imageName = $this->imageFileManager->getNewImageName($taskRequest->image);
        $this->imageFileManager->moveTo($taskRequest->image, $imageName);
        $this->imageFileManager->resizeImage($imageName, $this->imageWidth, $this->imageHeight);

        return $imageName;
    }
}
