<?php

namespace BeeJee\App\Service\Task;

use BeeJee\App\Model\Task\EditTaskRequest;
use BeeJee\App\Model\Task\Task;
use BeeJee\App\Repository\Task\TaskRepositoryInterface;

/**
 * Class EditTaskService
 * @package BeeJee\App\Task\Service
 */
class EditTaskService
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;


    /**
     * EditTaskService constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param EditTaskRequest $taskRequest
     * @return Task
     * @throws AddTaskValidationException
     */
    public function execute(EditTaskRequest $taskRequest)
    {
        $task = $this->taskRepository->getOneById($taskRequest->id);
        if ($taskRequest->status == Task::STATUS_INIT && !$task->isInProgress()) {
            $task->markAsInProgress();
        }
        if ($taskRequest->status == Task::STATUS_COMPLETE && !$task->isComplete()) {
            $task->markAsComplete();
        }
        $task->changeText($taskRequest->text);

        $this->taskRepository->save($task);

        return $task;
    }
}
