<?php

namespace BeeJee\App\Service;

class FileManager
{
    public function exists(string $path): bool
    {
        return file_exists($path);
    }

    public function delete(string $path): void
    {
        if (!file_exists($path)) {
            throw new \RuntimeException('Undefined path ');
        }

        if (is_dir($path)) {
            foreach (scandir($path, SCANDIR_SORT_ASCENDING) as $item) {
                if ($item === '.' || $item === '..') {
                    continue;
                }
                $this->delete($path.DIRECTORY_SEPARATOR.$item);
            }
            if (!rmdir($path)) {
                throw new \RuntimeException('Unable to delete directory ');
            }
        } else {
            if (!unlink($path)) {
                throw new \RuntimeException('Unable to delete file ');
            }
        }
    }

    public function moveUploadedFile(string $pathFrom, string $pathTo)
    {
        if (!move_uploaded_file($pathFrom, $pathTo)) {
            throw new \RuntimeException('Unable to move uploaded file');
        }
    }
}
