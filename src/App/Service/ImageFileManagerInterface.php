<?php

namespace BeeJee\App\Service;

use Zend\Diactoros\UploadedFile;

interface ImageFileManagerInterface
{
    public function moveUploadedImage(string $pathFrom, string $newImageName);

    public function moveTo(UploadedFile $image, string $newImageName);

    public function getNewImageName(UploadedFile $image);

    public function resizeImage(string $imageName, int $newWidth, int $newHeight);
}
