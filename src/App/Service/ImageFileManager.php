<?php

namespace BeeJee\App\Service;

use Zend\Diactoros\Exception\InvalidArgumentException;
use Zend\Diactoros\UploadedFile;

class ImageFileManager implements ImageFileManagerInterface
{
    /**
     * @var string
     */
    private $imagePath;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * ImageFileManager constructor.
     * @param string $imagePath
     * @param FileManager $fileManager
     */
    public function __construct(string $imagePath, FileManager $fileManager)
    {
        $this->imagePath = rtrim($imagePath, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        $this->fileManager = $fileManager;
    }

    public function moveUploadedImage(string $pathFrom, string $newImageName)
    {
        $pathTo = $this->imagePath.$newImageName;
        $this->fileManager->moveUploadedFile($pathFrom, $pathTo);
    }

    public function moveTo(UploadedFile $image, string $newImageName)
    {
        $pathTo = $this->imagePath.$newImageName;
        $image->moveTo($pathTo);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function getNewImageName(UploadedFile $file)
    {
        $imageName = uniqid();
        $metaData = $file->getStream()->getMetadata();
        $info = getimagesize($metaData['uri']);
        $extension = image_type_to_extension($info[2]);
        $extension = $extension === 'jpeg' ? 'jpg' : $extension;

        return sprintf('%s%s', $imageName, $extension);
    }

    public function resizeImage(string $imageName, int $maxWidth, int $maxHeight)
    {
        $path = $this->imagePath.$imageName;

        $mime = getimagesize($path);

        $imageWidth = $mime[0];
        $imageHeight = $mime[1];

        if ($imageWidth < $maxWidth && $imageHeight < $maxHeight) {
            return true;
        }

        $widthRatio = $maxWidth / $imageWidth;
        $heightRatio = $maxHeight / $imageHeight;

        // Ratio used for calculating new image dimensions.
        $ratio = min($widthRatio, $heightRatio);

        // Calculate new image dimensions.
        $newWidth = (int)$imageWidth * $ratio;
        $newHeight = (int)$imageHeight * $ratio;

        $srcImage = null;
        if ($mime['mime'] == 'image/png') {
            $srcImage = imagecreatefrompng($path);
        }
        if ($mime['mime'] == 'image/jpg' || $mime['mime'] == 'image/jpeg' || $mime['mime'] == 'image/pjpeg') {
            $srcImage = imagecreatefromjpeg($path);
        }

        if (!$srcImage) {
            throw new \InvalidArgumentException();
        }

        $oldX = imageSX($srcImage);
        $oldY = imageSY($srcImage);


        $dstImage = ImageCreateTrueColor($newWidth, $newHeight);

        imagecopyresampled($dstImage, $srcImage, 0, 0, 0, 0, $newWidth, $newHeight, $oldX, $oldY);

        $this->fileManager->delete($path);

        // New save location
        $newThumbFileName = $path;

        if ($mime['mime'] == 'image/png') {
            $result = imagepng($dstImage, $newThumbFileName, 8);
        }
        if ($mime['mime'] == 'image/jpg' || $mime['mime'] == 'image/jpeg' || $mime['mime'] == 'image/pjpeg') {
            $result = imagejpeg($dstImage, $newThumbFileName, 80);
        }

        imagedestroy($dstImage);
        imagedestroy($srcImage);


        return $result;
    }
}
