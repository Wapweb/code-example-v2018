<?php

namespace BeeJee\App\Controller;

use BeeJee\App\Controller\Decorator\AddTaskUrl;
use BeeJee\App\Controller\Decorator\IndexUrl;
use BeeJee\App\Model\Task\AddTaskRequest;
use BeeJee\App\Service\Task\AddTaskService;
use BeeJee\Framework\Controller\AbstractController;
use BeeJee\Framework\View\ViewInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class AddTaskController
 * @package BeeJee\App\Controller
 */
class AddTaskController extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var AddTaskService
     */
    private $addTaskService;

    /**
     * AddTaskController constructor.
     * @param AddTaskService $addTaskService
     * @param ViewInterface $view
     */
    public function __construct(AddTaskService $addTaskService, ViewInterface $view)
    {
        $this->addTaskService = $addTaskService;
        parent::__construct($view);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $method = $request->getMethod();
        $queryParams = $request->getQueryParams();

        if ($method === 'POST') {
            $addTaskRequest = AddTaskRequest::fromRequest($request);
            try {
                $this->addTaskService->execute($addTaskRequest);

                return new RedirectResponse((string)new IndexUrl([IndexUrl::QUERY_PARAM_SUCCESS => 1]));
            } catch (\Exception $e) {
                return new RedirectResponse((string)new AddTaskUrl(AddTaskUrl::QUERY_PARAM_ERROR));
            }
        }

        return new HtmlResponse($this->view->render('add-task', [
            'title' => 'Новая задача',
            'taskError' => isset($queryParams[AddTaskUrl::QUERY_PARAM_ERROR]),
        ]));
    }
}
