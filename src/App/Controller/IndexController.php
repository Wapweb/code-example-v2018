<?php

namespace BeeJee\App\Controller;

use BeeJee\App\Controller\Decorator\IndexUrl;
use BeeJee\App\Middleware\BasicAuthCheckLogin;
use BeeJee\App\Repository\Task\GetAllCriteria;
use BeeJee\App\Repository\Task\TaskRepositoryInterface;
use BeeJee\Framework\Controller\AbstractController;
use BeeJee\Framework\View\ViewInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;

class IndexController extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * IndexController constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param ViewInterface $view
     */
    public function __construct(TaskRepositoryInterface $taskRepository, ViewInterface $view)
    {
        $this->taskRepository = $taskRepository;
        parent::__construct($view);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryParams = $request->getQueryParams();
        $criteria = GetAllCriteria::fromRequest($request);
        $tasks = $this->taskRepository->getAllByCriteria($criteria);
        $tasksCount = $this->taskRepository->getAllCountByCriteria($criteria);

        if (!$this->pageIsValid($criteria->getPage(), $tasksCount, $criteria->getPerPage())) {
            return new RedirectResponse((string)new IndexUrl());
        }

        $viewData = [
            'tasks' => $tasks,
            'criteria' => $criteria,
            'tasksCount' => $tasksCount,
            'taskAdded' => isset($queryParams[IndexUrl::QUERY_PARAM_SUCCESS]),
            'taskEditError' => isset($queryParams[IndexUrl::QUERY_PARAM_EDIT_ERROR]),
            'taskEditSuccess' => isset($queryParams[IndexUrl::QUERY_PARAM_EDIT_SUCCESS]),
            'isAdmin' => $this->isAdmin($request)
        ];

        return new HtmlResponse($this->view->render('index', $viewData));
    }

    private function isAdmin(ServerRequestInterface $request)
    {
        return $request->getAttribute(BasicAuthCheckLogin::USER_ATTRIBUTE) !== null;
    }
}
