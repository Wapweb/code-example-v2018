<?php

namespace BeeJee\App\Controller;

use BeeJee\App\Controller\Decorator\AddTaskUrl;
use BeeJee\App\Controller\Decorator\IndexUrl;
use BeeJee\App\Model\Task\AddTaskRequest;
use BeeJee\App\Model\Task\EditTaskRequest;
use BeeJee\App\Service\Task\AddTaskService;
use BeeJee\App\Service\Task\EditTaskService;
use BeeJee\Framework\Controller\AbstractController;
use BeeJee\Framework\View\ViewInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class AddTaskController
 * @package BeeJee\App\Controller
 */
class EditTaskController extends AbstractController implements RequestHandlerInterface
{
    /**
     * @var EditTaskService
     */
    private $editTaskService;

    /**
     * AddTaskController constructor.
     * @param EditTaskService $editTaskService
     * @param ViewInterface $view
     */
    public function __construct(EditTaskService $editTaskService, ViewInterface $view)
    {
        $this->editTaskService = $editTaskService;
        parent::__construct($view);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $taskEditRequest = EditTaskRequest::fromRequest($request);
            $this->editTaskService->execute($taskEditRequest);
            return new RedirectResponse((string)new IndexUrl([IndexUrl::QUERY_PARAM_EDIT_SUCCESS => 1]));
        } catch (\Exception $e) {
            return new RedirectResponse((string)new IndexUrl([IndexUrl::QUERY_PARAM_EDIT_ERROR => 1]));
        }
    }
}
