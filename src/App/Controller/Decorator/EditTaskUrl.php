<?php

namespace BeeJee\App\Controller\Decorator;

/**
 * Class EditTaskUrl
 * @package BeeJee\App\Controller\Decorator
 */
class EditTaskUrl
{

    /**
     * @return string
     */
    public function __toString()
    {
        return '/edit-task/';
    }
}
