<?php

namespace BeeJee\App\Controller\Decorator;

/**
 * Class IndexUrl
 * @package BeeJee\App\Controller\Decorator
 */
class LoginUrl
{

    /**
     * @return string
     */
    public function __toString()
    {
        return '/?login';
    }
}
