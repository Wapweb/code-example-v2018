<?php

namespace BeeJee\App\Controller\Decorator;

/**
 * Class IndexUrl
 * @package BeeJee\App\Controller\Decorator
 */
class IndexUrl
{
    public const
        QUERY_PARAM_ERROR = 'error',
        QUERY_PARAM_SUCCESS = 'success',
        QUERY_PARAM_EDIT_ERROR = 'edit-error',
        QUERY_PARAM_EDIT_SUCCESS = 'edit-success';

    /**
     * @var array
     */
    private $queryParams;

    /**
     * AddTaskUrl constructor.
     * @param array $queryParams
     */
    public function __construct(array $queryParams = [])
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '/'.
            (!empty($this->queryParams) ? '?'.http_build_query($this->queryParams) : '');
    }
}
