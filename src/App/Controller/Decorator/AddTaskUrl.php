<?php

namespace BeeJee\App\Controller\Decorator;

/**
 * Class AddTaskUrl
 * @package BeeJee\App\Controller\Decorator
 */
class AddTaskUrl
{
    public const
        QUERY_PARAM_ERROR = 'error',
        QUERY_PARAM_SUCCESS = 'success';

    /**
     * @var bool
     */
    private $queryParam;

    /**
     * AddTaskUrl constructor.
     * @param string|null $queryParam
     */
    public function __construct(string $queryParam = null)
    {
        $this->queryParam = $queryParam;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '/add-task/'.
            ($this->queryParam ? '?'.$this->queryParam : '');
    }
}
