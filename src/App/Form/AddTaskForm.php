<?php

namespace BeeJee\App\Form;

use BeeJee\Framework\Form\FormInterface;
use BeeJee\Framework\Form\InvalidValueException;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\UploadedFile;

/**
 * Class AddTaskForm
 * @package BeeJee\App\Form
 */
class AddTaskForm implements FormInterface
{
    public const
        EL_NAME = 'username',
        EL_EMAIL = 'email',
        EL_TEXT = 'text',
        EL_IMAGE = 'image';

    private $data = [];

    private $elements = [
        self::EL_NAME,
        self::EL_EMAIL,
        self::EL_TEXT,
        self::EL_IMAGE,
    ];

    private $allowedImageTypes = [
        'image/jpeg',
        'image/png',
        'image/gif'
    ];

    private $maxImageSize = 1024*1024*5;

    /**
     * @param array $data
     * @return bool
     * @throws InvalidValueException
     */
    public function isValid($data): bool
    {
        if (!is_array($data)) {
            throw new InvalidValueException();
        }

        $this->data = $data;

        foreach ($this->elements as $el) {
            if (empty($this->data[$el])) {
                return false;
            }
        }

        // validate image
        /** @var UploadedFile $image */
        $image = $data[self::EL_IMAGE];

        if (!$image || !($image instanceof UploadedFile)) {
            throw new InvalidValueException();
        }

        if ($image->getError() !== 0) {
            return false;
        }

        if ($image->getSize() > $this->maxImageSize) {
            return false;
        }

        if (!in_array($image->getClientMediaType(), $this->allowedImageTypes)) {
            return false;
        }

        return true;
    }

    public function getErrors(): array
    {
        return ['Ошибка заполнения формы'];
    }

    /**
     * @return array
     */
    public function getValues()
    {
        $values = [];
        foreach ($this->elements as $el) {
            if (array_key_exists($el, $this->data)) {
                $values[$el] = $this->data[$el];
            }
        }

        return $values;
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    public static function getDataFromRequest(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        if (empty($params)) {
            return [];
        }

        $data[self::EL_NAME] = $params[self::EL_NAME] ?? null;
        $data[self::EL_TEXT] = $params[self::EL_NAME] ?? null;
        $data[self::EL_EMAIL] = $params[self::EL_NAME] ?? null;

        $files = $request->getUploadedFiles();
        $data[self::EL_IMAGE] = $files[self::EL_IMAGE] ?? null;

        return $data;
    }
}
