<?php

namespace BeeJee\App\Model\User;

/**
 * Class User
 * @package BeeJee\App\Model\User
 */
class User
{
    /**
     * @var bool
     */
    private $isAdmin;

    /**
     * User constructor.
     * @param bool $isAdmin
     */
    public function __construct($isAdmin = false)
    {
        $this->isAdmin;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @param bool $isAdmin
     * @return User
     */
    public static function fromParams(bool $isAdmin): User
    {
        return new self($isAdmin);
    }
}
