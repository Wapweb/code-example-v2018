<?php

namespace BeeJee\App\Model\Task;

/**
 * Class TaskImageUrlDecorator
 * @package BeeJee\App\Model\Task
 */
class TaskImageUrlDecorator
{
    /**
     * @var Task
     */
    private $task;

    /**
     * TaskImageUrlDecorator constructor.
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function __toString()
    {
        return sprintf('/assets/images/%s', $this->task->getImage());
    }
}
