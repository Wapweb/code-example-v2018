<?php

namespace BeeJee\App\Model\Task;

/**
 * Class TaskStatusTextDecorator
 * @package BeeJee\App\Model\Task
 */
class TaskStatusTextDecorator
{
    /**
     * @var Task
     */
    private $task;

    /**
     * TaskStatusTextDecorator constructor.
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function __toString()
    {
        $status = $this->task->getStatus();

        if ($status == Task::STATUS_INIT) {
            return 'в процессе';
        }

        if ($status == Task::STATUS_COMPLETE) {
            return 'выполнена';
        }

        return 'стаус неизвестен';
    }
}
