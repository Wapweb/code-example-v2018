<?php

namespace BeeJee\App\Model\Task;

use BeeJee\Framework\Model\AbstractModel;

class Task extends AbstractModel
{
    const STATUS_INIT = 0;
    const STATUS_COMPLETE = 10;

    /**
     * @var string|null
     */
    private $id;

    /**
     * @var string
     */
    private $userName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $image;

    /**
     * @var int
     */
    private $status;

    /**
     * Task constructor.
     * @param string $userName
     * @param string $email
     * @param string $text
     * @param int $status
     * @param int|null $id
     * @param string|null $image
     */
    public function __construct(
        string $userName,
        string $email,
        string $text,
        int $status,
        string $id = null,
        string $image = null
    ) {
        $this->userName = $userName;
        $this->email = $email;
        $this->text = $text;
        $this->id = $id;
        $this->status = $status;
        $this->image = $image;
    }

    /**
     * @param array $params
     * @return Task
     */
    public static function fromParams(array $params): Task
    {
        return new self(
            $params['username'] ?? null,
            $params['email'] ?? null,
            $params['text'] ?? null,
            $params['status'] ?? null,
            $params['id'] ?? null,
            $params['image'] ?? null
        );
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @throws AlreadyCompletedException
     */
    public function markAsComplete()
    {
        if ($this->isComplete()) {
            throw new AlreadyCompletedException();
        }
        $this->status = self::STATUS_COMPLETE;
    }

    /**
     * @throws AlreadyInProgressException
     */
    public function markAsInProgress()
    {
        if ($this->isInProgress()) {
            throw new AlreadyInProgressException();
        }
        $this->status = self::STATUS_INIT;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        return $this->status == self::STATUS_COMPLETE;
    }

    /**
     * @return bool
     */
    public function isInProgress()
    {
        return $this->status == self::STATUS_INIT;
    }

    /**
     * @param string $text
     */
    public function changeText(string $text)
    {
        $this->text = $text;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'username' => $this->userName,
            'email' => $this->email,
            'text' => $this->text,
            'image' => $this->image,
            'status' => $this->status,
        ];
    }
}
