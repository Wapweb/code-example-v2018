<?php

namespace BeeJee\App\Model\Task;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\UploadedFile;

/**
 * Class AddTaskRequest
 * @package BeeJee\App\Model\Task
 */
class AddTaskRequest
{
    /**
     * @var mixed
     */
    public $username;

    /**
     * @var mixed
     */
    public $text;

    /**
     * @var mixed
     */
    public $email;

    /**
     * @var null|UploadedFile
     */
    public $image;

    /**
     * @param ServerRequestInterface $request
     * @return AddTaskRequest
     */
    public static function fromRequest(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        $taskRequest = new self();
        if (empty($params)) {
            return $taskRequest;
        }

        $taskRequest->username = $params['username'] ?? null;
        $taskRequest->text = $params['text'] ?? null;
        $taskRequest->email = $params['email'] ?? null;

        $files = $request->getUploadedFiles();
        $taskRequest->image = $files['image'] ?? null;

        return $taskRequest;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'username' => $this->username,
            'text' => $this->text,
            'email' => $this->email,
            'image' => $this->image,
        ];
    }
}
