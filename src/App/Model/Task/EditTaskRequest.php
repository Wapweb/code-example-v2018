<?php

namespace BeeJee\App\Model\Task;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\UploadedFile;

/**
 * Class AddTaskRequest
 * @package BeeJee\App\Model\Task
 */
class EditTaskRequest
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int|null
     */
    public $status;

    /**
     * @param ServerRequestInterface $request
     * @return EditTaskRequest
     */
    public static function fromRequest(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        $taskRequest = new self();
        if (empty($params)) {
            return $taskRequest;
        }

        $taskRequest->id = $params['id'] ?? null;
        $taskRequest->text = $params['text'] ?? null;
        $taskRequest->status = $params['status'] ?? Task::STATUS_INIT;

        return $taskRequest;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'text' => $this->text,
            'id' => $this->id,
            'status' => $this->status,
        ];
    }
}
