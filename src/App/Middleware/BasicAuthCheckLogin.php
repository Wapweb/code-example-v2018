<?php

namespace BeeJee\App\Middleware;

use BeeJee\App\Controller\EditTaskController;
use BeeJee\App\Controller\IndexController;
use BeeJee\App\Controller\LoginController;
use Middlewares\BasicAuthentication;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class BasicAuth
 * @package BeeJee\App\Middleware
 */
class BasicAuthCheckLogin implements MiddlewareInterface
{
    public const USER_ATTRIBUTE = 'username';


    /**
     * @var BasicAuthentication
     */
    private $basicAuthentication;

    private $protectedControllers = [
        EditTaskController::class,
    ];

    /**
     * @var array
     */
    private $users = [];

    /**
     * BasicAuth constructor.
     * @param array $users
     * @param BasicAuthentication $basicAuthentication
     */
    public function __construct(array $users, BasicAuthentication $basicAuthentication)
    {
        $this->users = $users;
        $this->basicAuthentication = $basicAuthentication->attribute(self::USER_ATTRIBUTE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $username = $request->getServerParams()['PHP_AUTH_USER'] ?? null;
        $password = $request->getServerParams()['PHP_AUTH_PW'] ?? null;

        if ($this->alreadyLogged($username, $password)) {
            return $handler->handle($request->withAttribute(self::USER_ATTRIBUTE, $username));
        }

        if ($this->publicRequest($request)) {
            return $handler->handle($request);
        }

        return $this->basicAuthentication->process($request, $handler);
    }

    /**
     * @param null|string $username
     * @param null|string $password
     * @return bool
     */
    private function alreadyLogged(?string $username, ?string $password)
    {
        if (!empty($username) && !empty($password)) {
            foreach ($this->users as $name => $pass) {
                if ($username === $name && $password === $pass) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function publicRequest(ServerRequestInterface $request)
    {
        $queryParams = $request->getQueryParams();
        $publicController = !in_array(
            $request->getAttribute('request-handler'),
            $this->protectedControllers
        );

        return !isset($queryParams['login']) && $publicController;
    }
}
