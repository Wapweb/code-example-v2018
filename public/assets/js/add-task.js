$(function () {
    'use strict';

    function AddTask($el) {
        this.$el = $el;
        this.init();
    }

    AddTask.prototype.init = function () {
        this.$el
            .on('click', '#js-task-form-preview-btn', this.onPreviewBtnClick.bind(this))
            .on('click', '#js-task-form-close-preview-btn', this.onPreviewCloseBtnClick.bind(this));
    };

    AddTask.prototype.onPreviewBtnClick = function (e) {
        e.preventDefault();
        if (!this.$el[0].checkValidity()) {
            alert('Заполните форму');
            return;
        }

        this.showPreview();
    };

    AddTask.prototype.onPreviewCloseBtnClick = function (e) {
        e.preventDefault();
        this.hidePreview();
    };

    AddTask.prototype.showPreview = function () {
        var username = this.$el.find('#js-task-form-username').val(),
            email = this.$el.find('#js-task-form-email').val(),
            text = this.$el.find('#js-task-form-text').val(),
            $image = this.$el.find('#js-task-form-image'),
            $preview = this.$el.find('#js-task-form-preview');

        this.renderImageTo($image, $preview.find('.js-task-image'));

        $preview.find('.js-task-text').text(text);
        $preview.find('.js-task-username-text').text(username);
        $preview.find('.js-task-email-text').text(email);
        $preview.find('.js-task-status-text').text('в процессе');
        $preview.show();

        this.$el.find('#js-task-form-body').hide();
    };

    AddTask.prototype.hidePreview = function () {
        this.$el.find('#js-task-form-preview').hide();
        this.$el.find('#js-task-form-body').show();
    };

    AddTask.prototype.renderImageTo = function ($input, $to) {
        var input = $input[0];
        if (!input.files || !input.files[0]) {
            return false;
        }

        var reader = new FileReader();

        reader.onload = function (e) {
            $to.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    };

    // initialization
    new AddTask($('#js-task-form'));
});