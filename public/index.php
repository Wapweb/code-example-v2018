<?php

use BeeJee\Framework\Application;
use Relay\Relay;
use Zend\Diactoros\ServerRequestFactory;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

try {
    chdir(dirname(__DIR__));

    require_once 'vendor/autoload.php';

    $container = require_once 'config/container.php';
    $dispatcher = require_once 'config/routes.php';
    $middlewarePipes = require_once 'config/pipes.php';

    $requestHandler = new Relay($middlewarePipes);

    $app = new Application($container, $requestHandler);
    $response = $app->handle(ServerRequestFactory::fromGlobals());

    $emitter = new SapiEmitter();
    $emitter->emit($response);
} catch (\Exception $e) {
    echo $e->getMessage().'<br>';
    echo '<pre>'.$e->getTraceAsString().'</pre>';
}
