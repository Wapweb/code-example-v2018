#!/usr/bin/env bash

# Usage as a command:
#  ./install.sh userName userPassword

dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
config_template="$dir/../config/config.php.dist"
config_out="$dir/../config/config.php"
env_file="$dir/../.env"
skip_config=0

if [[ ! -f "${env_file}" ]]; then
    echo "ERROR: .env file '${env_file}' not found"
    exit 2
fi

if [[ ! -f "${config_template}" ]]; then
    echo "ERROR: config template '${config_template}' not found"
    exit 2
fi

if [[ -f "${config_out}" ]]; then
    skip_config=1
fi

source "$dir/templater.sh"

export $(cat $env_file | xargs)

if [[ ${skip_config} == 0 ]]; then
    echo "Create config.php in $config_out" 1>&2
    templater "$config_template" "$config_out"
fi

echo "Run composer install" 1>&2
composer install

echo "Success" 1>&2