<?php

use BeeJee\App\Middleware\BasicAuthCheckLogin;
use Middlewares\FastRoute;
use Middlewares\RequestHandler;
use Middlewares\TrailingSlash;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */

$middlewarePipes = [];
$middlewarePipes[] = $container->get(TrailingSlash::class);
$middlewarePipes[] = $container->get(FastRoute::class);
$middlewarePipes[] = $container->get(BasicAuthCheckLogin::class);
$middlewarePipes[] = $container->get(RequestHandler::class);

return $middlewarePipes;
