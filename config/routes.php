<?php

use BeeJee\App\Controller\EditTaskController;
use BeeJee\App\Controller\IndexController;
use BeeJee\App\Controller\AddTaskController;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

/** @var \DI\Container $container */

$dispatcher = simpleDispatcher(function (RouteCollector $r) {
    $r->get('/', IndexController::class);
    $r->get('/add-task/', AddTaskController::class);
    $r->post('/add-task/', AddTaskController::class);
    $r->post('/edit-task/', EditTaskController::class);
});

$container->set(Dispatcher::class, $dispatcher);
return $dispatcher;
