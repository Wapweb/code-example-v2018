<?php

use BeeJee\App\Form\AddTaskForm;
use BeeJee\App\Middleware\BasicAuthCheckLogin;
use BeeJee\App\Repository\Task\JsonFileTaskRepository;
use BeeJee\App\Repository\Task\TaskRepositoryInterface;
use BeeJee\App\Service\FileManager;
use BeeJee\App\Service\ImageFileManager;
use BeeJee\App\Service\ImageFileManagerInterface;
use BeeJee\App\Service\Task\AddTaskService;
use BeeJee\Framework\View\ViewInterface;
use BeeJee\Framework\View\DefaultView;
use DI\ContainerBuilder;
use function DI\create;
use function DI\factory;
use Middlewares\BasicAuthentication;
use Middlewares\RequestHandler;
use Middlewares\TrailingSlash;
use Psr\Container\ContainerInterface;

$builder = new ContainerBuilder();
$builder->useAutowiring(true);
$builder->useAnnotations(false);
$builder->addDefinitions([
    ViewInterface::class => factory(function (ContainerInterface $c) {
        return new DefaultView('views');
    }),

    BasicAuthentication::class => factory(function (ContainerInterface $c) {
        $config = $c->get('config');
        /** @var array $users */
        $users = $config['users'];

        return new BasicAuthentication($users);
    }),
    RequestHandler::class => factory(function (ContainerInterface $c) {
        return new RequestHandler($c);
    }),
    BasicAuthCheckLogin::class => factory(function (ContainerInterface $c) {
        $config = $c->get('config');
        /** @var array $users */
        $users = $config['users'];

        return new BasicAuthCheckLogin($users, $c->get(BasicAuthentication::class));
    }),
    TaskRepositoryInterface::class => factory(function (ContainerInterface $c) {
        return $c->get(JsonFileTaskRepository::class);
    }),
    ImageFileManagerInterface::class => factory(function (ContainerInterface $c) {
        return new ImageFileManager($c->get('config')['imagePath'], $c->get(FileManager::class));
    }),
    JsonFileTaskRepository::class => factory(function (ContainerInterface $c) {
        return new JsonFileTaskRepository($c->get('config')['taskDbFileName']);
    }),
    AddTaskService::class => factory(function (ContainerInterface $c) {
        return new AddTaskService(
            $c->get(JsonFileTaskRepository::class),
            $c->get(AddTaskForm::class),
            $c->get(ImageFileManagerInterface::class)
        );
    }),
    TrailingSlash::class => factory(function (ContainerInterface $c) {
        return (new Middlewares\TrailingSlash(true))
            ->redirect();
    }),
    'config' => require_once 'config/config.php',
]);

return $container = $builder->build();
